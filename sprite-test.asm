	org $ 8000

offscreen equ $c000 ; offscreen drawing area

	jp start

sprite_map
	ds 640 , 0

testship ; sprite 1
; ASM data file from a ZX-Paintbrush picture with 24 x 8 pixels (= 3 x 1 characters)
defb $00, $00, $03, $00, $00, $1F, $00, $01, $FF, $00, $0F, $FF, $00, $7F, $FF, $07
defb $FF, $FF, $3F, $FF, $FF, $FF, $FF, $FF

testbullet ; sprite 2 to 10
; ASM data file from a ZX-Paintbrush picture with 16 x 8 pixels (= 2 x 1 characters)
defb $00, $00, $00, $00, $00, $00, $03, $E0, $01, $F0, $03, $E0, $00, $00, $00, $00


clear_map 
	ld hl,sprite_map
	ld de,sprite_map+1
	ld bc, 639 ; one less than the map size
	ld (hl),0
	ldir
	ret

clear_char
	push af
	push hl
	ld b,8
cc_loop
	ld (hl),a
	inc h
	djnz cc_loop
	pop hl
	pop af
	ret

back_char

	push hl
	push de
	push hl
	pop de
	ld a,d
	or $80
	ld d,a
	ld b,8
bd_loop
	ld a,(de)
	ld (hl),a
	inc d
	inc h
	djnz bd_loop
	pop de
	pop hl
	ret


refresh


	ld hl,$4000
	ld de,sprite_map
	ld bc,640
ref_loop
	push bc
	push de
	push hl

	ld a,(de)
	and a ; is it zero
	call z,clear_char
	call nz,back_char

	pop hl
	inc hl
	ld a,l
	and a
	jr nz,sameseg
	bit 0,h
	jr z, sameseg
	
	ld a,h
	dec a
	add a,8
	ld h,a

sameseg
	pop de
	inc de
	pop bc
	dec bc
	ld a,b
	or c
	jr nz, ref_loop
	ret


shipx db 4
shipy db 13


update_ship
	
	ld a,(shipy)
	ld l,a
	ld h,0
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,hl
	ld a,(shipx)
	ld d,0
	ld e,a
	add hl,de
	ex de,hl ; de holds the offset

	ld hl,sprite_map
	add hl,de  
;hl point to sprite_map location

	ld a,1
	ld (hl),a
	inc hl
	ld (hl),a
	inc hl
	ld (hl),a
	
	ld hl,$5800 ; attribs
	add hl,de ; this is the attib for the screen

	call attr2backdisp

	ld hl,testship
	ld b,8
ship_pixels

	push de
	ldi
	ldi
	ldi
	pop de
	inc d
	djnz ship_pixels

	ret
	

attr2backdisp 
	xor a
	ld a,h
	and 3
	rla
	rla
	rla
	or $c0
	ld d,a
	ld e,l
	ret


flags db 0  	; bit 0 - left (a)
		; bit 1 - right (d)
		; bit 2-  up  (w)
		; bit 3 - down (s)
		; but 4 - fire (SPACE)


keyboard
		ld hl,flags
		ld (hl),0
		ld bc, 65022
		in a,(c)
left
		push af
		bit 0,a ; (A)
		jr nz,down
		set 0,(hl)

down
		pop af
		push af
		bit 1,a
		jr nz,right
		set 3,(hl)

right
		pop af
		push af

		bit 2,a ; (D)
		jr nz,up
		set 1,(hl)
up
		pop af
		ld bc, 64510
		in a,(c)
		bit 1,a
		jr nz,fire
		set 2,(hl)

fire 		ld bc, 32766
		in a,(c)
		bit 0,a
		jr nz,done
		set 4,(hl)
done		ret




movesprite

	ld hl,flags
	ld a,(hl)
mleft
	rra
	jp nc,mright

	push af
	ld a,(shipx)
	and a
	jr z,ignoreleft

	dec a
	ld (shipx),a

ignoreleft pop af



mright	rra
	jr nc,mup

	push af
	ld a,(shipx)
	cp 29
	jr z,ignoreright

	inc a
	ld (shipx),a

ignoreright pop af




mup	rra
	jr nc,mdown

	push af
	ld a,(shipy)
	and a
	jr z,ignoreup
	dec a
	ld (shipy),a

ignoreup pop af



mdown	rra	
	jr nc,mfire

	push af
	ld a,(shipy)
	cp 19
	jr z,ignoredown

	inc a
	ld (shipy),a

ignoredown pop af



mfire	rrca
; to be done
	call c,firebullet

	ret
	



firearray dw 0
	  dw 0
	  dw 0
          dw 0
	  dw 0
          dw 0
	  dw 0
	  dw 0
	  dw 0


movebullets

	ld bc,$0902 ; b is the counter - c is the bullet reference
	ld ix,firearray


arrayloop
	push bc
	ld a,(ix+1)
	or (ix+0)
	call nz,moveit
	pop bc
	inc ix
	inc ix
	inc c
	djnz arrayloop
	ret

moveit	
	ld a,(ix+1)
	cp 29
	jr nc,deletebullet
	inc a
	inc a
	ld (ix+1),a
	ret

deletebullet
	xor a
	ld (ix+0),a
	ld (ix+1),a
	ret



firebullet

	ld bc,$0902 ; b is the counter - c is the bullet reference
	ld ix,firearray


fireloop

	ld a,(ix+1)
	or (ix+0)
	jp z,firethisone
	inc ix
	inc ix
	inc c
	djnz fireloop

	ret ; all bullets used so ignore it

firethisone

	ld hl,(shipx)
	ld a,l
	add a,1
	cp 30
	ret nc
	ld (ix+1),a
	ld (ix+0),h
	ret


update_bullets


	ld bc,$0902 ; b is the counter - c is the bullet reference
	ld ix,firearray


showloop
	push bc
	ld a,(ix+1)
	or (ix+0)
	call nz,showbullet
	pop bc
	inc ix
	inc ix
	inc c
	djnz showloop
	ret


showbullet

	ld a,(ix+0)
	ld l,a
	ld h,0
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,hl
	ld a,(ix+1)
	ld d,0
	ld e,a
	add hl,de
	ex de,hl ; de holds the offset

	ld hl,sprite_map
	add hl,de  
;hl point to sprite_map location

	ld a,c
	ld (hl),a
	inc hl
	ld (hl),a
	
	ld hl,$5800 ; attribs
	add hl,de ; this is the attib for the screen

	call attr2backdisp

	ld hl,testbullet
	ld b,8
bul_pixels

	push de
	ldi
	ldi
	pop de
	inc d
	djnz bul_pixels

	ret




;*************



	
start
	call clear_map
	call update_ship
	call update_bullets
	call keyboard
	halt
	call refresh

	call movesprite
	call movebullets

	ld bc, 64510
	in a,(c)
	bit 0,a
	jr nz,start

	ret
	
	


